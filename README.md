# My personal discord bot
This is a discord bot I've made for fun, the code is a big mess and has to be cleaned, it's one of my first projects using nodejs.

## It has the following features:

1. Google search
2. Youtube video search
3. Cleverbot API integrated
4. Google Translate API
5. Programming documentation (searches google for tutorialspoint references)
