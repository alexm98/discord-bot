const Discord = require("discord.js");
var fs = require('fs');

// functions so that i don't have to write boilerplate code
var functions = {};
    functions.getRandomColor = function(){
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    functions.post_FromFile = function(file,embedheader,message){
        fs.readFile(file,'utf8',function(err,res){
            var embedcolor = functions.getRandomColor();
            if(err) return console.log(err);
            var embed = new Discord.RichEmbed().setColor(embedcolor);
            embed.addField(embedheader,res);
            message.channel.send(embed);
        });
    }

    functions.generateRandomNumber = function(max){
        if(max == null){
            return Math.floor(Math.random()*100);
        }
        else{
            return Math.floor(Math.random()*max);
        }
    }

exports.data = functions;
