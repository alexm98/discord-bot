const Discord = require("discord.js");

var fs = require('fs');
var google = require('google');
var translate = require('google-translate-api');
var youtube = require('youtube-search');
var cleverbot = require('cleverbot.io');
var sys = require('util');
var exec = require('child_process').exec;

const client = new Discord.Client();
const prefix = ['ck/ ','ck/'];

// API keys
var keys = JSON.parse(fs.readFileSync('api_keys.json','utf8'));

// functions in separate file
var functions = require('./functions.js');

var embedcolor = functions.data.getRandomColor();
var bot = new cleverbot(keys['cleverbot1'],keys['cleverbot2']);

// when the bot is ready
client.on("ready", () => {
      console.log("I am ready!");
      client.user.setGame('lainchan.org');
});

client.on("message", function(message) {
    if(message.author.equals(client.user)) return;
    // without using prefix
    switch(message.content){
        case 'REEE':
            message.channel.send('REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE');
            message.channel.send('http://i0.kym-cdn.com/photos/images/original/000/915/652/b49.gif');
            break;
        case 'kek':
            message.channel.send('top kek');
            break;
        default:
        if(message.channel.id == '383004121025871872'){
            bot.setNick("gradinserver");

            bot.create(function(err,session){
                bot.ask(message.content,function(err,response){
                    message.channel.send(response);
                });
            });
        }
        break;
    }

	if(message.content.startsWith(prefix[0]))
	    var args= message.content.substring(prefix[0].length).split(" ");
	else if(message.content.startsWith(prefix[1]))
	    var args= message.content.substring(prefix[1].length).split(" ");
	else
	    return;

    switch(args[0]){
        case 'online?':
            message.channel.send("Yes I'm here");
            break;
	case 'help':
            functions.data.post_FromFile('help.txt','Help for cerealkiller-BOT | **Usage: ck/ <command>**',message);
            break;
        case 'gsearch':
            var searchstring = args.slice(1).join(' ');
            google.resultsPerPage = 25;

            google(searchstring, function (err, res){
                if (err) console.error(err);
                var link = res.links[0];

                var embed = new Discord.RichEmbed().setColor(embedcolor);
                embed.addField(link.title,link.href + '\n\n' + link.description);
                message.channel.send(embed);
            });
        case 'translate':
            if(args[1] == 'help'){
                message.channel.send("For a list of available languages, go to : " +"https://cloud.google.com/translate/docs/languages");
            }
            else{
                var untranslated = args.slice(2).join(' ');
                translate(untranslated, {to: args[1]}).then(res => {
                    message.channel.send(res.text);
                }).catch(err => {
                    message.channel.send("Language is not supported.");
                });
            }
            break;
        case 'ysearch':
            var opts = {
                maxResults: 1,
                key: keys['youtube'],
                nullvall : null
            };

            var searchstring = args.slice(1).join(' ');
            youtube(searchstring,opts,function(err,res){
                if(err) return console.log(err);

                message.channel.send(res[0].link);
            });
            break;
        case 'clever':
            var searchstring = args.slice(1).join(' ');
            bot.setNick("gradinserver");

            bot.create(function(err,session){
                bot.ask(searchstring,function(err,response){
                    message.channel.send(response);
                });
            });
            break;
        case 'resources':
            functions.data.post_FromFile('resources.txt','Some cool resources',message);
            break;
        case 'documentatie':
            var searchstring = args.slice(1).join(' ');
            google.resultsPerPage = 25;

            google(searchstring+' site:tutorialspoint.com', function (err, res){
                if(err) return console.log(err);
                var link = res.links[0];

                if(res.links.length > 0){
                    var embed = new Discord.RichEmbed().setColor(embedcolor);
                    embed.addField(link.title,link.href + '\n\n' + link.description);
                    message.channel.send(embed);
                }
                else{
                    message.channel.send("Haven't found anything about '"+searchstring+"' on tutorialspoint.");
                }
            });
            break;

        //memes, other stuff
        case 'REEE':
            message.channel.send('REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE');
            message.channel.send('http://i0.kym-cdn.com/photos/images/original/000/915/652/b49.gif');
            break;
        case 'GNU/Linux':
            message.channel.send('https://en.wikipedia.org/wiki/GNU/Linux_naming_controversy');
            break;
        case 'fortune':
            child = exec("fortune | cowsay", function (error, stdout, stderr) {
                message.channel.send('```' + stdout + '```');
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
            break;
        case 'roll':
            message.channel.send(functions.data.generateRandomNumber(args[1]));
            break;
        case 'middle':
            functions.data.post_FromFile('middle.txt',args[1],message);
            break;
        case 'writecode':
            message.channel.send('https://media1.tenor.com/images/f552638ff2e4099790a44c1ccf245301/tenor.gif');
            break;
        default:
            message.channel.send('Invalid command!');
            break;
    }
});

client.login(keys['discord']);
